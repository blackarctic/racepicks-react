export function validateEmail(email) {
  if (!email || !email.length) {
    return {
      error: 'Email cannot be blank',
    };
  }

  const atIndex = email.lastIndexOf('@');
  const dotIndex = email.lastIndexOf('.');

  if (
    atIndex === -1 ||
    dotIndex === -1 ||
    atIndex > dotIndex ||
    atIndex === 0 ||
    dotIndex === email.length - 1 ||
    atIndex + 1 === dotIndex
  ) {
    return {
      error: 'Email format is invalid',
    };
  }

  if (email.length > 100) {
    return {
      error: 'Email is too long',
    };
  }

  return { error: null };
}

export function validatePassword(password) {
  if (!password || !password.length) {
    return {
      error: 'Password cannot be blank',
    };
  }

  if (password.length > 100) {
    return {
      error: 'Password is too long',
    };
  }

  return { error: null };
}
