export const FirebaseMock = {
  database: () => ({
    ref: () => Promise.resolve(),
  }),
  auth: () => ({
    signInWithEmailAndPassword: () => Promise.resolve(),
    signOut: () => Promise.resolve(),
    sendPasswordResetEmail: () => Promise.resolve(),
    onAuthStateChanged: () => Promise.resolve(),
  }),
};
