export function childAtLevel(componentTree, n) {
  let component = componentTree;
  for (let i = 0; i < n; i += 1) {
    component = component.childAt(0);
  }
  return component;
}
