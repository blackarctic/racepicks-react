import {
  USER_LOGIN,
  USER_LOGOUT,
  USER_LOAD,
} from '../../../actions/UserActions';

import {
  loginUser,
  logoutUser,
} from '../../../../resources/UserResource';

const initState = {
  authCompleted: false,
  user: null,
};

export const UserReducer = (state = initState, action) => {
  switch (action.type) {
    case USER_LOGIN:
      loginUser(action.email, action.password);
      return {};
    case USER_LOGOUT:
      logoutUser();
      return {};
    case USER_LOAD:
      return { authCompleted: true, user: action.payload };
    default:
      return state;
  }
};
