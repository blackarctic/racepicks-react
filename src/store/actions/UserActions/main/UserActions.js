// Action types
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_LOAD = 'USER_LOAD';

// Action creators
export const loadUser = user => (
  { type: USER_LOAD, payload: user }
);
export const loginUser = (email, password) => (
  {
    type: USER_LOGIN,
    payload: {
      email,
      password,
    },
  }
);
export const logoutUser = () => (
  { type: USER_LOGOUT }
);
