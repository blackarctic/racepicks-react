import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import { UserReducer } from '../reducers/UserReducer';
import {
  LoginFormReducer as LoginPage_LoginFormReducer,
} from '../../pages/LoginPage/components/LoginForm/store/LoginFormReducer';

const combinedReducer = combineReducers({
  user: UserReducer,
  loginPage_loginForm: LoginPage_LoginFormReducer,
});

export const Store = createStore(
  combinedReducer,
  composeWithDevTools(applyMiddleware(thunk)),
);
