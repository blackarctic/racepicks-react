import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';

import { onUserAuthChange } from '../../../resources/UserResource';
import { loadUser } from '../../../store/actions/UserActions';

import { FormsHeader } from '../components/FormsHeader';
import { FormsFooter } from '../components/FormsFooter';
import { LoginForm } from '../components/LoginForm';
import { RegisterForm } from '../components/RegisterForm';
import { ResetPasswordForm } from '../components/ResetPasswordForm';

import '../styles/LoginPage.css';

class LoginPage extends React.Component {
  componentWillMount() {
    onUserAuthChange((user) => {
      this.props.loadUser(user);
    });
  }

  render() {
    document.title = this.props.title;
    if (!this.props.authCompleted) {
      return <div />;
    }
    else {
      if (!this.props.user) {
        return (
          <div className="LoginPage">
            <div className="LoginPage_Body">
              <div className="LoginPage_Forms text-center">
                <FormsHeader />
                { this.props.match.path === '/login' && <LoginForm /> }
                { this.props.match.path === '/register' && <RegisterForm /> }
                { this.props.match.path === '/forgot-password' && <ResetPasswordForm /> }
                <FormsFooter path={this.props.match.path} />
              </div>
            </div>
          </div>
        );
      }
      else {
        return <Redirect to="/" />;
      }
    }
  }
}

LoginPage.propTypes = {
  title: PropTypes.string.isRequired,
  match: PropTypes.shape({
    path: PropTypes.string,
  }).isRequired,
  user: PropTypes.object,
  authCompleted: PropTypes.bool.isRequired,
  loadUser: PropTypes.func.isRequired,
};

LoginPage.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: state.user.user,
  authCompleted: state.user.authCompleted,
});
const mapDispatchToProps = { loadUser };

const ConnectedLoginPage = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginPage));
export {
  ConnectedLoginPage as LoginPage,
  LoginPage as LoginPageRaw,
};
