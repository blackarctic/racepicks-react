import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const FormsFooter = (props) => {
  let linkTo;
  let linkText;

  if (props.path === '/login') {
    linkTo = '/register';
    linkText = 'Need an account?';
  }
  else if (props.path === '/register') {
    linkTo = '/login';
    linkText = 'Have an account?';
  }
  else if (props.path === '/forgot-password') {
    linkTo = '/login';
    linkText = 'Return to log in';
  }

  return (
    <div>
      <div className="LoginPage_Forms_SectionDivider" />
      <div className="form-group">
        <Link to={linkTo} className="button-link">
          <button type="button" className="btn btn-secondary btn-block">{linkText}</button>
        </Link>
      </div>
    </div>
  );
};

FormsFooter.propTypes = {
  path: PropTypes.string.isRequired,
};

export { FormsFooter };
