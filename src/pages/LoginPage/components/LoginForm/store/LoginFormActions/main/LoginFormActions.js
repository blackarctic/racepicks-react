import { loginUser } from '../../../../../../../resources/UserResource';
import {
  validateEmail,
  validatePassword,
} from '../../../../../../../services/ValidationService';

// Action types
export const LOGIN_FORM_RESET = 'LOGIN_FORM_RESET';
export const LOGIN_FORM_SET_EMAIL = 'LOGIN_FORM_SET_EMAIL';
export const LOGIN_FORM_SET_PASSWORD = 'LOGIN_FORM_SET_PASSWORD';
export const LOGIN_FORM_SET_EMAIL_ERROR = 'LOGIN_FORM_SET_EMAIL_ERROR';
export const LOGIN_FORM_SET_PASSWORD_ERROR = 'LOGIN_FORM_SET_PASSWORD_ERROR';
export const LOGIN_FORM_SUBMIT = 'LOGIN_FORM_SUBMIT';
export const LOGIN_FORM_IN_PROGRESS = 'LOGIN_FORM_IN_PROGRESS';
export const LOGIN_FORM_SUCCEEDED = 'LOGIN_FORM_SUCCEEDED';
export const LOGIN_FORM_FAILED = 'LOGIN_FORM_FAILED';
export const LOGIN_FORM_CLEAR_RESULT = 'LOGIN_FORM_FAILED_CLEAR';

// Action creators
export const resetLoginForm = () => (
  { type: LOGIN_FORM_RESET }
);
export const setLoginFormEmail = email => (
  { type: LOGIN_FORM_SET_EMAIL, payload: email }
);
export const setLoginFormPassword = password => (
  { type: LOGIN_FORM_SET_PASSWORD, payload: password }
);
export const setLoginFormEmailError = emailErr => (
  { type: LOGIN_FORM_SET_EMAIL_ERROR, payload: emailErr }
);
export const setLoginFormPasswordError = passwordErr => (
  { type: LOGIN_FORM_SET_PASSWORD_ERROR, payload: passwordErr }
);
export const succeedLoginForm = () => (
  { type: LOGIN_FORM_SUCCEEDED }
);
export const failLoginForm = err => (
  { type: LOGIN_FORM_FAILED, payload: err }
);
export const clearLoginFormResult = () => (
  { type: LOGIN_FORM_CLEAR_RESULT }
);

// Action dispatchers
export const submitLoginForm = () => {
  return async function (dispatch, getState) {
    // get state
    let {
      emailVal,
    } = getState().loginPage_loginForm;
    const {
      passwordVal,
    } = getState().loginPage_loginForm;

    // cleanup inputs
    emailVal = emailVal.trim().toLowerCase();

    // validate inputs
    const emailValidation = validateEmail(emailVal);
    const passwordValidation = validatePassword(passwordVal);
    dispatch(setLoginFormEmailError(emailValidation.error));
    dispatch(setLoginFormPasswordError(passwordValidation.error));

    // if inputs are valid, submit the form
    if (
      !emailValidation.error &&
      !passwordValidation.error
    ) {
      try {
        await loginUser(emailVal, passwordVal);
        dispatch(succeedLoginForm());
      }
      catch (err) {
        const submissionResultTimeout = setTimeout(() => {
          dispatch(clearLoginFormResult());
        }, 2000);
        dispatch(failLoginForm(submissionResultTimeout));

        switch (err.code) {
          case 'auth/user-not-found':
            dispatch(setLoginFormEmailError('No user found with this email'));
            break;
          default:
            // TODO show an alert with the server error message
        }
      }
    }
    else {
      const submissionResultTimeout = setTimeout(() => {
        dispatch(clearLoginFormResult());
      }, 2000);
      dispatch(failLoginForm(submissionResultTimeout));
    }
  };
};
