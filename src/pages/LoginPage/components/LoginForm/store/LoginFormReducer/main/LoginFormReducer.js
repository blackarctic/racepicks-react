import {
  LOGIN_FORM_RESET,
  LOGIN_FORM_SET_EMAIL,
  LOGIN_FORM_SET_PASSWORD,
  LOGIN_FORM_SET_EMAIL_ERROR,
  LOGIN_FORM_SET_PASSWORD_ERROR,
  LOGIN_FORM_IN_PROGRESS,
  LOGIN_FORM_SUCCEEDED,
  LOGIN_FORM_FAILED,
  LOGIN_FORM_CLEAR_RESULT,
} from '../..';

const initState = {
  emailVal: '',
  emailError: null,

  passwordVal: '',
  passwordError: null,

  submissionInProgress: false,
  submissionFailed: false,
  submissionResultTimeout: null,
};

export const LoginFormReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_FORM_RESET:
      return initState;
    case LOGIN_FORM_SET_EMAIL:
      return {
        ...state,
        emailVal: action.payload,
        emailError: null,
      };
    case LOGIN_FORM_SET_PASSWORD:
      return {
        ...state,
        passwordVal: action.payload,
        passwordError: null,
      };
    case LOGIN_FORM_SET_EMAIL_ERROR:
      return {
        ...state,
        emailError: action.payload,
      };
    case LOGIN_FORM_SET_PASSWORD_ERROR:
      return {
        ...state,
        passwordError: action.payload,
      };
    case LOGIN_FORM_IN_PROGRESS:
      clearTimeout(state.submissionResultTimeout);
      return {
        ...state,
        emailError: null,
        passwordError: null,
        submissionInProgress: true,
        submissionFailed: false,
        submissionResultTimeout: null,
      };
    case LOGIN_FORM_SUCCEEDED:
      return {
        ...state,
        emailVal: '',
        passwordVal: '',
        submissionInProgress: false,
      };
    case LOGIN_FORM_FAILED:
      return {
        ...state,
        submissionInProgress: false,
        submissionFailed: true,
        submissionResultTimeout: action.payload,
      };
    case LOGIN_FORM_CLEAR_RESULT:
      return {
        ...state,
        submissionFailed: false,
        submissionResultTimeout: null,
      };
    default:
      return state;
  }
};
