import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';

import {
  resetLoginForm,
  setLoginFormEmail,
  setLoginFormPassword,
  submitLoginForm,
} from '../store';

class LoginForm extends React.Component {
  componentWillMount() {
    this.props.resetLoginForm();
  }

  handleEmailInputChange(e) {
    const val = e.target.value;
    this.props.setLoginFormEmail(val);
  }
  handlePasswordInputChange(e) {
    const val = e.target.value;
    this.props.setLoginFormPassword(val);
  }
  handleFormSubmit(e) {
    e.preventDefault();
    this.props.submitLoginForm();
  }

  getFormButton() {
    if (this.props.submissionInProgress) {
      return (
        <button
          type="submit"
          disabled
          className="btn btn-primary btn-block"
        >
          Processing...
        </button>
      );
    }
    else if (this.props.submissionFailed) {
      return (
        <button
          type="submit"
          className="btn btn-danger btn-block"
        >
          Login Failed
        </button>
      );
    }
    else {
      return (
        <button
          type="submit"
          className="btn btn-primary btn-block"
        >
          Log In
        </button>
      );
    }
  }

  render() {
    const formButton = this.getFormButton();
    return (
      <form
        onSubmit={e => this.handleFormSubmit(e)}
        noValidate
      >
        <div className="form-group text-left">
          <input
            type="email"
            className={
              `
                form-control
                ${this.props.emailError && 'is-invalid'}
              `
            }
            id="emailInput"
            placeholder="Email"
            value={this.props.emailVal}
            onChange={e => this.handleEmailInputChange(e)}
          />
          {
            this.props.emailError &&
            <div className="invalid-feedback">
              {this.props.emailError}
            </div>
          }
        </div>
        <div className="form-group text-left">
          <input
            type="password"
            className={
              `
                form-control
                ${this.props.passwordError && 'is-invalid'}
              `
            }
            id="passwordInput"
            placeholder="Password"
            value={this.props.passwordVal}
            onChange={e => this.handlePasswordInputChange(e)}
          />
          {
            this.props.passwordError &&
            <div className="invalid-feedback">
              {this.props.passwordError}
            </div>
          }
        </div>
        <div className="form-group">
          <Link to="/forgot-password">
            <button type="button" className="btn btn-link">
              <small>Forgot your password?</small>
            </button>
          </Link>
        </div>
        <div className="form-group">
          {formButton}
        </div>
      </form>
    );
  }
}

LoginForm.propTypes = {
  submissionInProgress: PropTypes.bool.isRequired,
  submissionFailed: PropTypes.bool.isRequired,
  emailVal: PropTypes.string.isRequired,
  passwordVal: PropTypes.string.isRequired,
  emailError: PropTypes.string,
  passwordError: PropTypes.string,

  resetLoginForm: PropTypes.func.isRequired,
  submitLoginForm: PropTypes.func.isRequired,
  setLoginFormEmail: PropTypes.func.isRequired,
  setLoginFormPassword: PropTypes.func.isRequired,
};

LoginForm.defaultProps = {
  emailError: null,
  passwordError: null,
};

const mapStateToProps = state => ({
  submissionInProgress: state.loginPage_loginForm.submissionInProgress,
  submissionFailed: state.loginPage_loginForm.submissionFailed,
  emailVal: state.loginPage_loginForm.emailVal,
  passwordVal: state.loginPage_loginForm.passwordVal,
  emailError: state.loginPage_loginForm.emailError,
  passwordError: state.loginPage_loginForm.passwordError,
});
const mapDispatchToProps = {
  resetLoginForm,
  submitLoginForm,
  setLoginFormEmail,
  setLoginFormPassword,
};

const ConnectedLoginForm = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginForm));
export { ConnectedLoginForm as LoginForm };
