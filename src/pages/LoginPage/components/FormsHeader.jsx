import React from 'react';

const FormsHeader = () => (
  <div>
    <h3>RacePicks</h3>
    <div className="LoginPage_Forms_SectionDivider" />
  </div>
);

export { FormsHeader };
