import React from 'react';

const ResetPasswordForm = () => (
  <form>
    <div className="form-group">
      <input
        type="email"
        className="form-control"
        id="emailInput"
        placeholder="Email"
      />
    </div>
    <div className="form-group">
      <button type="submit" className="btn btn-primary btn-block">Request Password Reset</button>
    </div>
  </form>
);

export { ResetPasswordForm };
