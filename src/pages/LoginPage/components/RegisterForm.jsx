import React from 'react';

const RegisterForm = () => (
  <form>
    <div className="form-group">
      <input
        type="username"
        className="form-control"
        id="usernameInput"
        placeholder="Username"
      />
    </div>
    <div className="form-group">
      <input
        type="email"
        className="form-control"
        id="emailInput"
        placeholder="Email"
      />
    </div>
    <div className="form-group">
      <input
        type="password"
        className="form-control"
        id="passwordInput"
        placeholder="Password"
      />
    </div>
    <div className="form-group">
      <button type="submit" className="btn btn-primary btn-block">Create Account</button>
    </div>
  </form>
);

export { RegisterForm };
