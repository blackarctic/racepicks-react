/* globals describe it expect */

import { configure, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import { MemoryRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { Store } from '../../../store';
import { childAtLevel } from '../../../testing/services/TestingService';
import { FirebaseMock } from '../../../testing/mocks/FirebaseMock';

import { LoginPageRaw } from '../main/LoginPage';

global.firebase = FirebaseMock;

// Configure Enzyme for use with Jest & ES6+
configure({ adapter: new Adapter() });


const wrapper = (props, {
  path = '/login',
  title = 'Login page',
} = {}) => (
  <Provider store={Store}>
    <MemoryRouter
      initialEntries={[path]}
      keyLength={0}
    >
      <Switch>
        <Route
          path={path}
          render={
            routeProps => (
              <LoginPageRaw
                title={title}
                loadUser={() => {}}
                {...routeProps}
                {...props}
              />
            )
          }
        />
        <Route
          exact
          path="/"
          render={
            () => <div className="FakeHomePage" />
          }
        />
      </Switch>
    </MemoryRouter>
  </Provider>
);

const doMount = (props, path) => {
  return childAtLevel(mount(wrapper(props, path)), 5);
};


describe('LoginPage - Login', () => {
  it('renders expected markup when auth succeeds', () => {
    expect(toJson(doMount(
      {
        authCompleted: true,
        user: {
          username: 'user123',
          email: 'user123@email.com',
        },
      },
    ))).toMatchSnapshot();
  });

  it('renders expected markup when auth fails', () => {
    expect(toJson(doMount(
      {
        authCompleted: true,
        user: null,
      },
    ))).toMatchSnapshot();
  });

  it('renders expected markup when auth is pending', () => {
    expect(toJson(doMount(
      {
        authCompleted: false,
        user: null,
      },
    ))).toMatchSnapshot();
  });
});


describe('LoginPage - Register', () => {
  it('renders expected markup when auth succeeds', () => {
    expect(toJson(doMount(
      {
        authCompleted: true,
        user: {
          username: 'user123',
          email: 'user123@email.com',
        },
      },
      {
        path: '/register',
        title: 'Login Page - Register',
      },
    ))).toMatchSnapshot();
  });

  it('renders expected markup when auth fails', () => {
    expect(toJson(doMount(
      {
        authCompleted: true,
        user: null,
      },
      {
        path: '/register',
        title: 'Login Page - Register',
      },
    ))).toMatchSnapshot();
  });

  it('renders expected markup when auth is pending', () => {
    expect(toJson(doMount(
      {
        authCompleted: false,
        user: null,
      },
      {
        path: '/register',
        title: 'Login Page - Register',
      },
    ))).toMatchSnapshot();
  });
});


describe('LoginPage - Forgot Password', () => {
  it('renders expected markup when auth succeeds', () => {
    expect(toJson(doMount(
      {
        authCompleted: true,
        user: {
          username: 'user123',
          email: 'user123@email.com',
        },
      },
      {
        path: '/forgot-password',
        title: 'Login Page - Forgot Password',
      },
    ))).toMatchSnapshot();
  });

  it('renders expected markup when auth fails', () => {
    expect(toJson(doMount(
      {
        authCompleted: true,
        user: null,
      },
      {
        path: '/forgot-password',
        title: 'Login Page - Forgot Password',
      },
    ))).toMatchSnapshot();
  });

  it('renders expected markup when auth is pending', () => {
    expect(toJson(doMount(
      {
        authCompleted: false,
        user: null,
      },
      {
        path: '/forgot-password',
        title: 'Login Page - Forgot Password',
      },
    ))).toMatchSnapshot();
  });
});
