import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';

import { onUserAuthChange } from '../../../resources/UserResource';
import { loadUser } from '../../../store/actions/UserActions';

import '../styles/HomePage.css';

class HomePage extends React.Component {
  componentWillMount() {
    onUserAuthChange((user) => {
      this.props.loadUser(user);
    });
  }

  render() {
    document.title = this.props.title;
    if (!this.props.authCompleted) {
      return <div />;
    }
    else {
      if (this.props.user) {
        return (
          <div className="HomePage">
            <h1>Welcome to the Home Page, {this.props.user.email}!</h1>
          </div>
        );
      }
      else {
        return <Redirect to="/login" />;
      }
    }
  }
}

HomePage.propTypes = {
  title: PropTypes.string.isRequired,
  user: PropTypes.object,
  authCompleted: PropTypes.bool.isRequired,
  loadUser: PropTypes.func.isRequired,
};

HomePage.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: state.user.user,
  authCompleted: state.user.authCompleted,
});
const mapDispatchToProps = { loadUser };

const ConnectedHomePage = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage));
export {
  ConnectedHomePage as HomePage,
  HomePage as HomePageRaw,
};
