/* globals describe it expect */

import { configure, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import { MemoryRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { Store } from '../../../store';
import { childAtLevel } from '../../../testing/services/TestingService';
import { FirebaseMock } from '../../../testing/mocks/FirebaseMock';

import { HomePageRaw } from '../main/HomePage';

global.firebase = FirebaseMock;

// Configure Enzyme for use with Jest & ES6+
configure({ adapter: new Adapter() });


const wrapper = (props, { path = '/' } = {}) => (
  <Provider store={Store}>
    <MemoryRouter
      initialEntries={[path]}
      keyLength={0}
    >
      <Switch>
        <Route
          exact
          path={path}
          render={
            routeProps => (
              <HomePageRaw
                title="Home Page"
                loadUser={() => {}}
                {...routeProps}
                {...props}
              />
            )
          }
        />
        <Route
          exact
          path="/login"
          render={
            () => <div className="FakeLoginPage" />
          }
        />
      </Switch>
    </MemoryRouter>
  </Provider>
);

const doMount = (props) => {
  return childAtLevel(mount(wrapper(props)), 5);
};


describe('HomePage', () => {
  it('renders expected markup when auth succeeds', () => {
    expect(toJson(doMount(
      {
        authCompleted: true,
        user: {
          username: 'user123',
          email: 'user123@email.com',
        },
      },
    ))).toMatchSnapshot();
  });

  it('renders expected markup when auth fails', () => {
    expect(toJson(doMount(
      {
        authCompleted: true,
        user: null,
      },
    ))).toMatchSnapshot();
  });

  it('renders expected markup when auth is pending', () => {
    expect(toJson(doMount(
      {
        authCompleted: false,
        user: null,
      },
    ))).toMatchSnapshot();
  });
});
