/* globals describe it expect */

import { configure, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import { MemoryRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { Store } from '../../../store';
import { childAtLevel } from '../../../testing/services/TestingService';
import { FirebaseMock } from '../../../testing/mocks/FirebaseMock';

import { NotFound404PageRaw } from '../main/NotFound404Page';

global.firebase = FirebaseMock;

// Configure Enzyme for use with Jest & ES6+
configure({ adapter: new Adapter() });


const wrapper = (props, { path = '/' } = {}) => (
  <Provider store={Store}>
    <MemoryRouter
      initialEntries={[path]}
      keyLength={0}
    >
      <Switch>
        <Route
          render={
            routeProps => (
              <NotFound404PageRaw
                title="404 - Page Not Found"
                loadUser={() => {}}
                {...routeProps}
                {...props}
              />
            )
          }
        />
      </Switch>
    </MemoryRouter>
  </Provider>
);

const doMount = (props) => {
  return childAtLevel(mount(wrapper(props)), 5);
};


describe('NotFound404Page', () => {
  it('renders expected markup', () => {
    expect(toJson(doMount())).toMatchSnapshot();
  });
});
