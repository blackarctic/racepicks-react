import React from 'react';
import PropTypes from 'prop-types';

import '../styles/NotFound404Page.css';

const NotFound404Page = (props) => {
  document.title = props.title;
  return (
    <div className="NotFound404Page">
      <h1>Not Found!</h1>
    </div>
  );
};

NotFound404Page.propTypes = {
  title: PropTypes.string.isRequired,
};

export {
  NotFound404Page,
  NotFound404Page as NotFound404PageRaw,
};
