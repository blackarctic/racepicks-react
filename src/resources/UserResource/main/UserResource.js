/* globals firebase */

import axios from 'axios';
import { extractRes } from '../../../services/ResService';

export function createUser(username, email, password) {
  return new Promise((resolve, reject) => {
    axios.post('https://us-central1-racepicks-2.cloudfunctions.net/apiv1/user', {
      username,
      email,
      password,
    }).then((res) => {
      resolve(extractRes(res));
    }).catch((err) => {
      if (!err.response) { reject(err); }
      reject(extractRes(err.response));
    });
  });
}

export function getUser(userId) {
  return new Promise((resolve, reject) => {
    return firebase.database().ref(`users/${userId}`).once('value')
      .then((snapshot) => {
        resolve(snapshot.val() || {});
      }).catch(reject);
  });
}

export function loginUser(email, password) {
  return firebase.auth()
    .signInWithEmailAndPassword(email, password);
}

export function logoutUser() {
  return firebase.auth().signOut();
}

export function resetUserPassword(email) {
  return firebase.auth()
    .sendPasswordResetEmail(email);
}

export function onUserAuthChange(cb) {
  return firebase.auth().onAuthStateChanged(cb);
}
