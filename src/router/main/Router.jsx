import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { HomePage } from '../../pages/HomePage';
import { LoginPage } from '../../pages/LoginPage';
import { NotFound404Page } from '../../pages/NotFound404Page';

const Router = () => (
  <BrowserRouter>
    <Switch>

      <Route
        exact
        path="/"
        render={props => <HomePage title="RacePicks" {...props} />}
      />
      <Route
        path="/home"
        render={props => <HomePage title="RacePicks" {...props} />}
      />

      <Route
        path="/login"
        render={props => <LoginPage title="Login | RacePicks" {...props} />}
      />
      <Route
        path="/register"
        render={props => <LoginPage title="Register | RacePicks" {...props} />}
      />
      <Route
        path="/forgot-password"
        render={props => <LoginPage title="Forgot Password | RacePicks" {...props} />}
      />

      <Route
        render={props => <NotFound404Page title="404 Not Found" {...props} />}
      />
    </Switch>
  </BrowserRouter>
);

export { Router };
