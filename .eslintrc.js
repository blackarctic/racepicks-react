module.exports = {
    "extends": "airbnb",
    "env": {
      "browser": true,
      "node": true,
      "es6": true
    },
    "rules": {
      "react/sort-comp": false,
      "react/forbid-prop-types": false,
      "import/prefer-default-export": false,
      "jsx-a11y/anchor-is-valid": false,
      "consistent-return": 0,
      "brace-style": [
        "error",
        "stroustrup",
        {
          "allowSingleLine": true
        }
      ],
      "prefer-destructuring": 0,
      "arrow-body-style": 0,
      "newline-per-chained-call": 0,
      "no-lonely-if": 0,
      "no-else-return": 0,
      "func-names": 0,
      "function-paren-newline": 0,
      "object-curly-newline": 0,
      "camelcase": 0,
    }
};
